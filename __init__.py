# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import company
from . import sale
from . import account


def register():
    Pool.register(
        company.Company,
        company.ConnectionCompanies,
        sale.Sale,
        sale.SaleIncomeDailyStart,
        # sale.SaleMonthByShopStart,
        # sale.SaleGoalAnnualRankingStart,
        sale.SaleDetailedStart,
        sale.ShopDailySummaryStart,
        account.PrintBalanceSheetCOLGAAPStart,
        account.PrintIncomeStatementCOLGAAPStart,
        account.PrintTrialBalanceStart,
        module='syncronize_companies', type_='model')
    Pool.register(
        account.TrialBalanceClassic,
        # sale.SaleDailyReport,
        sale.SaleDetailedReport,
        sale.PortfolioDetailedReport,
        sale.ExpiredPortfolioReport,
        sale.PortfolioBySalesmanReport,
        sale.MovesInvoicesReport,
        account.BalanceSheetCOLGAAP,
        account.IncomeStatementCOLGAAP,
        module='syncronize_companies', type_='report')
    Pool.register(
        account.PrintTrialBalance,
        account.PrintBalanceSheetCOLGAAP,
        account.PrintIncomeStatementCOLGAAP,
        module='syncronize_companies', type_='wizard')
