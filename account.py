# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
import requests
import simplejson as json
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

_ZERO = Decimal('0.00')


class PrintTrialBalanceStart(metaclass=PoolMeta):
    __name__ = 'account.print_trial_balance.start'
    sincronize = fields.Boolean('Syncronize')


class PrintTrialBalance(metaclass=PoolMeta):
    __name__ = 'account.print_trial_balance'

    def do_print_(self, action):
        action, data = super(PrintTrialBalance, self).do_print_(action)
        data['sincronize'] = self.start.sincronize
        return action, data


class TrialBalanceClassic(metaclass=PoolMeta):
    __name__ = 'account_col.trial_balance_classic'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        result = {}
        sync = data['sincronize']
        print('yyyyyyyyyyy', sync)
        if sync:
            data['sincronize'] = False
            args = {
                'records': [],
                'data': data,
            }
            for sync in company.connection_companies:
                api_ = 'http://' + sync.api_connection + '/' + sync.database
                route = api_ + '/' + 'report_context'
                data_ = json.dumps({
                        'database': sync.database,
                        'report_name': 'account_col.trial_balance_classic',
                        'args': args,
                    })
                res = requests.get(route, data=data_)
                result = res.json()
                # obj_ = dict(sorted(result['records'].items()))
                print('-' * 190)
                print(result)
                print('-' * 190)
                report_context['accounts'].extend(result)

                # report_context['records'].extend(obj_.values())
                # report_context['global_result'] += Decimal(result['global_result'])
        return report_context


class PrintIncomeStatementCOLGAAPStart(metaclass=PoolMeta):
    __name__ = 'account_col.print_income_statement_colgaap.start'
    sincronize = fields.Boolean('Syncronize')


class PrintIncomeStatementCOLGAAP(metaclass=PoolMeta):
    __name__ = 'account_col.print_income_statement_colgaap'

    def do_print_(self, action):
        action, data = super(PrintIncomeStatementCOLGAAP, self).do_print_(action)
        data['sincronize'] = self.start.sincronize
        return action, data


class IncomeStatementCOLGAAP(metaclass=PoolMeta):
    __name__ = 'account_col.income_statement_colgaap'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        result = {}
        sync = data['sincronize']
        if sync:
            data['sincronize'] = [False]

            args = {
                'records': [],
                'data': data,
            }
            for sync in company.connection_companies:
                api_ = 'http://' + sync.api_connection + '/' + sync.database
                route = api_ + '/' + 'report_context'
                data_ = json.dumps({
                        'database': sync.database,
                        'report_name': 'account_col.income_statement_colgaap',
                        'args': args,
                    })
                res = requests.get(route, data=data_)
                result = res.json()
                obj_ = dict(sorted(result['records'].items()))
                print(report_context['records'])
                print('-'*190)
                print(obj_.values())
                report_context['records'].extend(obj_.values())
                report_context['global_result'] += Decimal(result['global_result'])
        return report_context


class PrintBalanceSheetCOLGAAPStart(metaclass=PoolMeta):
    __name__ = 'account_col.print_balance_sheet_colgaap.start'
    sincronize = fields.Boolean('Syncronize')


class PrintBalanceSheetCOLGAAP(metaclass=PoolMeta):
    __name__ = 'account_col.print_balance_sheet_colgaap'

    def do_print_(self, action):
        action, data = super(PrintBalanceSheetCOLGAAP, self).do_print_(action)
        data['sincronize'] = self.start.sincronize
        return action, data


class BalanceSheetCOLGAAP(metaclass=PoolMeta):
    __name__ = 'account_col.balance_sheet_colgaap'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        result = {}
        sync = data['sincronize']
        if sync:
            data['sincronize'] = [False]

            args = {
                'records': records,
                'data': data,
            }
            for sync in company.connection_companies:
                api_ = 'http://' + sync.api_connection + '/' + sync.database
                route = api_ + '/' + 'report_context'
                data_ = json.dumps({
                            'database': sync.database,
                            'report_name': 'account_col.balance_sheet_colgaap',
                            'args': args,
                        })
                res = requests.get(route, data=data_)
                result = res.json()
                obj_ = dict(sorted(result['records'].items()))
                report_context['records'].extend(obj_.values())
                report_context['global_result'] += Decimal(result['global_result'])
        return report_context
